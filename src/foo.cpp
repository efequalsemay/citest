#include "foo.hpp"

#include <iostream>

void Foo::print() {
  std::cout << "Hello, world" << std::endl;
}
